#include "mbed.h"
#include "rtos.h"
#include "hts221.h"
#include "LPS25H.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string>

/**
Class Measurement

Object to store all the data measured by the sensor

@param tempC - Temperature from sensor in degrees celsius
@param humidity - Humidity from sensor
@param temp - Bar temperature
@param pressure - Bar pressure
@param dateString - String representation of the data and time the measurement was taken
*/
class Measurement {
	public: float tempC;
	public: float humidity;
	public: float pressure;
	public: float temp;
	public: char* dateString;
		
		public: Measurement(float tempC, float humidity, float temp, float pressure, char* dateString) {
		this->tempC = tempC;
		this->humidity = humidity;
		this->temp = temp;
		this->pressure = pressure;
		this->dateString = dateString;
	}
};

DigitalOut myled(LED1);
I2C i2c2(I2C_SDA, I2C_SCL);

float tempCelsius = 25.50;
float humi = 55;
int humiMax = 100; 
float tInterval = 5.0;

//State for the system date - default to UNIX Epoch
int year = 1970;
int month = 1;
int day = 1;
int hour = 0;
int minute = 0;
int seccond = 0;

uint32_t seconds = 0, minutes=0, hours=0; 
LPS25H barometer(i2c2, LPS25H_V_CHIP_ADDR);

//Initialise ISR
void doReadData();

//Initialise methods
void readAll();
void readN(int n);
void deleteAll();
void deleteN(int n);
void setT(float T);
void state(bool setState);

//Set up a ticker to measure data
Ticker measurementTicker;
bool isOn = false;

//Set up mail box to read and store data in thread safe way
Mail<Measurement, 16> mail_box;

//Circular buffer
Measurement* measurements[120];
MemoryPool<Measurement, 120> measurement_pool;

int measurementIndex = 0;
int oldestIndex = 0;

/**
Method to convert a string to an integer

@param str - String to convert
@return - Returns integer of the string
*/
int convertStrToInt(char* str) {
	long int n = 0;
	n = strtol(str, NULL, 10);
	return (int) n;
}

/**
Thread to handle serial input/output. Accepts commands from user, interprets and executes
*/
void serialThread( const void* arg) {
	while(true) {
		printf("\r\nEnter command: ");

		//Initialise command input arrays
		int i = 0;
		char command[250];
		char fullCommand[10][25];
		
		//Clear all input arrays
		for (int j = 0; j < sizeof(command); j++) {
			command[j] = 0;
		}
		
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 25; k++) 
			{
				fullCommand[j][k] = 0;
			}
		}
	
		//User input
		while (command[i] != 13) {
			command[i] = getchar();
		
			if (command[i] == 13) { //13 is ASCII code for new line
				printf("\n\r");
				break;
			} 
			else if (command[i] == 127) { //127 is ASCII code for backspace
				command[i - 1] = 0; //Remove previous character from array
				command[i] = 0; //Removes backspace character from array
				i = i - 2;
			}
			
			i++;
		}
	
		//Add a space to the end to allow split to work with only one word
		command[i] = 32;
	
		char* pointer;
		int index = 0;
		
		//Delimit on spaces and decimal points
		pointer = strtok(command, " .");
		
		while (pointer != NULL) {
			strcpy(fullCommand[index], pointer);
			pointer = strtok(NULL, " .");
			index++;
		}
		
		index = 0;
		
		//"?" command shows information about the system
		if(strcmp(fullCommand[0], "?") == 0) {
			printf("SOFT253 simple Temperature Humidity and Pressure Sensor Monitor\n\r");
			printf("Using the X-NUCLEO-IKS01A1 shield and MBED Libraries\n\r");
		}
	
		//"READ" command displays results - displays all is ALL specified, otherwise displays N recent results
		if(strcmp(fullCommand[0], "READ") == 0) {
			if (strcmp(fullCommand[1], "ALL") == 0) {
				readAll();
			} 
			else {
				long int n = 0;
				
				n = convertStrToInt(fullCommand[1]);
				
				if (n > 0 && n < 121) {
					printf("%s is not a number, or is out of the range of 1 - 120", fullCommand[1]);
				} 
				else {
					readN(n);
				}
			}
		//"DELETE" command deletes results - deletes all is ALL specified, otherwise deletes N oldest results
		} else if (strcmp(fullCommand[0], "DELETE") == 0) {
			if (strcmp(fullCommand[1], "ALL") == 0) {
				deleteAll();
			} else {
				long int n = 0;
				
				n = convertStrToInt(fullCommand[1]);
				
				if (n > 0 && n < 121) {
					printf("%s is not a number, or is out of the range of 1 - 120", fullCommand[1]);
				} else {
					deleteN(n);
				}
			}
		//"SETT" command allows the user to set the sampling interval
		} else if (strcmp(fullCommand[0], "SETT") == 0) {
			long int n = 0;
			long int y = 0;
			float tValue = 0.0;
				
			n = convertStrToInt(fullCommand[1]);
			y = convertStrToInt(fullCommand[2]);
		
			tValue = ((float)y / 10) + n;
		
			setT(tValue);
		//"STATE" command starts or stops sampling depending on if ON or OFF is used
		} else if (strcmp(fullCommand[0], "STATE") == 0) {
			bool setState = false;
		
			if (strcmp(fullCommand[1], "ON") == 0) {
				setState = true;
				state(setState);
			} else if (strcmp(fullCommand[1], "OFF") == 0) {
				setState = false;
				state(setState);
			}
		//"SETDATE" Sets the date to the user's input of DD MM YYYY
		} else if (strcmp(fullCommand[0], "SETDATE") == 0) {
			day = (int)convertStrToInt(fullCommand[1]);
			month = (int)convertStrToInt(fullCommand[2]);
			year = (int)convertStrToInt(fullCommand[3]);
		
			printf("Date updated to: %02d/%02d/%4d", day, month, year);
		//"SETTIME" Sets the time to the user's input of HH MM SS
		} else if (strcmp(fullCommand[0], "SETTIME") == 0) {
			hour = (int)convertStrToInt(fullCommand[1]);
			minute = (int)convertStrToInt(fullCommand[2]);
			seccond = (int)convertStrToInt(fullCommand[3]);
			printf("Time updated to: %02d:%02d:%02d", hour, minute, seccond);
		}
	}
}

/**
Thread to handle taking measurements from the environmental sensor
*/
void measurementThread( const void* arg) {
	//Set ISR function to ticker at interval of tInterval
	measurementTicker.attach(&doReadData, tInterval);
	isOn = true;

	while(true) {	
		sleep();
	
		//Get measurement from ISR
		osEvent evt = mail_box.get(5000);
		
		if (evt.status == osEventMail) {
			Measurement *pMeasure = (Measurement*) evt.value.p;
			
			//Create copy of measurement
			Measurement measure(pMeasure->tempC, pMeasure->humidity, pMeasure->temp, pMeasure->pressure, pMeasure->dateString);
			
			//Add measurement to circular buffer
			if (measurementIndex < 120) {
				Measurement	*measurement = measurement_pool.alloc();
				
				measurement->tempC = measure.tempC;
				measurement->humidity = measure.humidity;
				measurement->temp = measure.temp;
				measurement->pressure = measure.pressure;
				measurement->dateString = measure.dateString;
				
				measurements[measurementIndex] = measurement;
				
				measurementIndex++;
			} else {	
				osStatus status = measurement_pool.free(measurements[oldestIndex]);
				
				if (status != osErrorValue) {
					Measurement	*measurement = measurement_pool.alloc();
					
					measurement->tempC = measure.tempC;
					measurement->humidity = measure.humidity;
					measurement->temp = measure.temp;
					measurement->pressure = measure.pressure;
					measurement->dateString = measure.dateString;
					
					measurements[oldestIndex] = measurement;
					
					if (oldestIndex < 120) {
							oldestIndex++;
					} else {
							oldestIndex = 0;
					}
				}
			}	
			
			//Free up memory from the mail box when finished
			mail_box.free(pMeasure);
		}
	}
}

/**
Main function - sets up threads, doesn't do anything else really
*/
int main() {
	hts221_init();
	HTS221_Calib();
	printf("SOFT253 simple Temperature Humidity and Pressure Sensor Monitor\n\r");
	printf("Using the X-NUCLEO-IKS01A1 shield and MBED Libraries\n\r");
		
	Thread threadMeasurement(measurementThread);
	Thread threadSerial(serialThread);

	while(1) {
			sleep();	
	}
}

/**
ISR to measure data from the environmental sensor
*/
void doReadData() {	
	//Test sample data because mbed broke the driver
	//I have no idea if the data is safe environmental conditions
	float tempC = 12;
	float humidity = 70;
	float temp = 10;
	float pressure = 9;
	
	//Allocate memory in the mail box
	Measurement *measure = mail_box.alloc();
	
	//Set values into the memory address
	measure -> temp = temp;
	measure -> tempC = tempC;
	measure -> humidity = humidity;
	measure -> pressure = pressure;
	
	char dateString[50];
	snprintf(dateString, sizeof(dateString), "%02d:%02d:%02d %02d/%02d/%4d", hour, minute, seccond, day, month, year);

	measure -> dateString = dateString;
	
	//Post measurement to mail box - picked up by measurementThread
	osStatus status = mail_box.put(measure);
	
	//Handle errors caused by overfilled mail box
	if (status == osErrorResource) {
		mail_box.free(measure);
		return;
	}
}

/**
Read all records
*/
void readAll() {
	if (measurementIndex != 120) {
		for (int i = measurementIndex -1; i >= oldestIndex; i--) {				
			Measurement measure = *measurements[i];
			printf("%4.2fC %3.1f%% %6.1f %4.1f %s\r\n", measure.tempC, measure.humidity, measure.pressure, measure.temp, measure.dateString);
		}
	} else {
		int index = oldestIndex - 1;
		
		for (int i = measurementIndex -1; i >= 0; i--) {
			Measurement measure = *measurements[index];
			printf("%4.2fC %3.1f%% %6.1f %4.1f%% %s\r\n", measure.tempC, measure.humidity, measure.pressure, measure.temp, measure.dateString);

			if (index <= 0) {
					index = 120;
			}
			
			index--;
		}
	}
}

/**
Delete all records
*/
void deleteAll() {
	for (int i = measurementIndex -1; i >= 0; i--) {
		Measurement* pMeasure = measurements[i];
		osStatus status = measurement_pool.free(pMeasure);
		
		if (status == osErrorValue) {
				printf("An error occured deleting the records\r\n");
				return;
		}
	}
	measurementIndex = 0;
	oldestIndex = 0;
	printf("All records deleted\r\n");
}

/**
Read N records
@param n - Number of records to read
*/
void readN(int n) {
	if (measurementIndex < n) {		
		readAll();			
	} else {
		if (oldestIndex == 0) {
			int index = measurementIndex -1;
			for (int i = index; i > index - n; i--) {
				Measurement measure = *measurements[i];
				printf("%4.2fC %3.1f%% %6.1f %4.1f %s\r\n", measure.tempC, measure.humidity, measure.pressure, measure.temp, measure.dateString);
			}	
		} else {
			int index = oldestIndex -1;
			for (int i = index; i > index - n; i--) {
				Measurement measure = *measurements[index];
				printf("%4.2fC %3.1f%% %6.1f %4.1f %s\r\n", measure.tempC, measure.humidity, measure.pressure, measure.temp, measure.dateString);
			
				if (i == 0) {
					i = 120;
				}
			}	
		}
	}
}

/**
Delete N records
@param n - Number of records to delete
*/
void deleteN(int n) {
	if (measurementIndex < n) {		
		printf("There are only %d record(s) stored, all will be deleted\r\n", measurementIndex -1);
		deleteAll();			
	} else {				
		int index = oldestIndex;
		for (int i = index; i < index + n; i++) {
			Measurement* pMeasure = measurements[i];
			osStatus status = measurement_pool.free(pMeasure);
			
			if (status == osErrorValue) {
				printf("An error occured deleting the records\r\n");
				return;
			}
		}
		
		printf("%d records deleted\r\n", n);
	}
		
	if (measurementIndex == 120) {
		if (oldestIndex > 120 - n) {
			oldestIndex = ((n + oldestIndex) - 120);
		} else {
			oldestIndex += n;
		}
	} else {
		oldestIndex += n;
	}
}

/**
Set the sampling interval.
@param T - The sampling rate in seconds. Accepts only floats between 0.1 and 60.0
*/
void setT(float T) {
	if (T >= 0.1 && T <= 60.0) {
		tInterval = T;
		printf("T updated to: %2f", tInterval);
	} else {
		printf("T must be in the range of 0.1 - 60.0");
	}
	
	if (isOn) {
		//Reset the ticker with the updated sampling interval
		measurementTicker.detach();

		measurementTicker.attach(&doReadData, tInterval);
	}
}

/**
Set state of the system (turn it off and on again)
@param setState - Boolean value for the intended state of the device
*/
void state(bool setState) {
	if (isOn != setState) {
		if (!setState) {
			measurementTicker.detach();
			isOn = false;
			printf("Sampling stopped");
		} else {
			measurementTicker.attach(&doReadData, tInterval);
			isOn = true;
			printf("Sampling started");
		}
	}
}
